Use OnePlace_Data;

/****** Object:  Table [dbo].[Object_Truncation]    Script Date: 02/23/2016 09:33:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Object_Truncation]') AND type in (N'U'))
DROP TABLE [dbo].[Object_Truncation]
GO

/****** Object:  Table [dbo].[Object_Truncation]    Script Date: 28/08/2015 12:15:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Object_Truncation](
	[OB_Name] [nvarchar](50) NOT NULL,
	[OB_Order] [int] NOT NULL,
	[OB_Exception] [nvarchar](max) NULL,
 CONSTRAINT [PK_Object_Truncation] PRIMARY KEY CLUSTERED 
(
	[OB_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'Account', 21, N'''00124000008FpkVAAS'',''00124000008FpkUAAS''')
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'Attachment', 11, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'Contact', 20, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'Event', 7, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'Note', 10, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__C_Address__c', 14, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Company_Address__c', 13, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Company_Group__c', 1, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Function__c', 3, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Invitee__c', 2, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__List__c', 5, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__List_Member__c', 4, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Matter__c', 12, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Meeting__c', 18, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Meeting_Contact__c', 17, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Other_Company_Contact__c', 9, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Publication__c', 16, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Publication_Subscriber__c', 15, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'OnePlace__Relationship__c', 8, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'Opportunity', 19, NULL)
GO
INSERT [dbo].[Object_Truncation] ([OB_Name], [OB_Order], [OB_Exception]) VALUES (N'Task', 6, NULL)
GO

-- Declare the local variables
DECLARE @objectSelect NVARCHAR(255)
DECLARE @objectDelete NVARCHAR(255)
DECLARE @exception NVARCHAR(MAX)
DECLARE @sql NVARCHAR(MAX)
DECLARE @sqlProgress NVARCHAR(MAX)

-- Set the local variables
SET @sqlProgress = ''

-- Create a cursor to loop the objects
DECLARE object_cursor CURSOR FOR
SELECT
	OB_Name, OB_Exception
FROM	
	Object_Truncation
ORDER BY
	OB_Order

-- Open the cursor
OPEN object_cursor

-- Get the first record
FETCH NEXT FROM object_cursor INTO @objectSelect, @exception

-- Loop while we have objects
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Set the delete variable
	SET @objectDelete = @objectSelect + '_Delete'

	-- Check to see if the table exists, if not create it
	IF NOT EXISTS (SELECT * FROM sys.tables WHERE Name = @objectDelete AND type = 'U')
	BEGIN
		SET @sql = 'CREATE TABLE ' + @objectDelete + ' (Id NCHAR(18) PRIMARY KEY, Error NVARCHAR(255) NULL)'
		EXEC sp_executesql @sql
	END

	-- Clear the table
	SET @sql = 'TRUNCATE TABLE ' + @objectDelete
	EXEC sp_executesql @sql

	-- Refresh the data pre-delete
	SET @sql = 'EXEC SF_Replicate ''OnePlace'', ''' + @objectSelect + ''''
	EXEC sp_executesql @sql

	-- Append the data
	SET @sql = 'INSERT INTO ' + @objectDelete + ' (Id) SELECT Id FROM ' + @objectSelect + CASE WHEN ISNULL(@exception, '') = '' THEN '' ELSE ' WHERE Id NOT IN(' + @exception + ')' END
	EXEC sp_executesql @sql

	-- Run the delete
	SET @sql = 'EXEC SF_BulkOps ''DELETE'', ''OnePlace'', ''' + @objectDelete + ''''
	EXEC sp_executesql @sql

	-- Refresh the data post delete
	SET @sql = 'EXEC SF_Replicate ''OnePlace'', ''' + @objectSelect + ''''
	EXEC sp_executesql @sql

	-- Select the data
	IF @sqlProgress = ''
		BEGIN
			SET @sqlProgress = 'SELECT ''' + @objectSelect + ''' AS [Object_Name], COUNT(*) AS Remaining_Records FROM ' + @objectSelect
		END
	ELSE
		BEGIN
			SET @sqlProgress = @sqlProgress + ' UNION SELECT ''' + @objectSelect + ''' AS [Object_Name], COUNT(*) AS Remaining_Records FROM ' + @objectSelect
		END
	
	-- Get the next record
	FETCH NEXT FROM object_cursor INTO @objectSelect, @exception
END

-- Close and deallocate the cursor
CLOSE object_cursor
DEALLOCATE object_cursor

-- Get the final report
EXEC sp_executesql @sqlProgress