
-- Create a backup of the table first.
EXEC SF_Replicate 'OnePlace','Account';
GO

EXEC sp_rename 'Account', 'Account_BACKUP';  
GO 

-- Create a temporary table for the keys to be used for deletion.
IF OBJECT_ID('dbo.Account_Delete', 'U') IS NOT NULL
    DROP TABLE dbo.Account_Delete;

CREATE TABLE [dbo].Account_Delete(
	[Id] [nchar](18) NULL,
	[Error] [nvarchar](255) NULL
) ON [PRIMARY]

INSERT INTO [dbo].Account_Delete
(
			[Id]
)
SELECT      opa.ID AS [ID]
FROM	
        oneplace...account opa join OP_ClientGrouping g on opa.OnePlace__Client_Code__c COLLATE Latin1_General_CS_AS = g.rclnum AND g.clnum != g.rclnum

-- Execute the delete (for security this line is commented out until required.)
-- EXEC sf_bulkops 'Delete','ONEPLACE','Account_Delete'