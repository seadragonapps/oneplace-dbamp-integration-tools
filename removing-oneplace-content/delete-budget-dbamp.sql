
-- Create a backup of the table first.
SF_Replciate 'OnePlace', 'OnePlace__Budget__c';
GO

Select *
Into OnePlace__Budget__c_22March2017
From OnePlace__Budget__c

-- Create a temporary table for the keys to be used for deletion.
IF OBJECT_ID('dbo.OnePlace__Budget__c_Delete', 'U') IS NOT NULL
    DROP TABLE dbo.OnePlace__Budget__c_Delete;

CREATE TABLE [dbo].OnePlace__Budget__c_Delete(
	[Id] [nchar](18) NULL,
	[Error] [nvarchar](255) NULL
) ON [PRIMARY]

INSERT INTO [dbo].OnePlace__Budget__c_Delete
(
			[Id]
)
SELECT	OpM.ID AS [ID]
FROM	
        oneplace...OnePlace__Budget__c AS OpM

-- Execute the delete (for security this line is commented out until required.)
-- EXEC sf_bulkops 'Delete','ONEPLACE','OnePlace__Budget__c_Delete'