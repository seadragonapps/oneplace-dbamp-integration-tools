
-- Create a backup of the table first.
EXEC SF_Replicate 'OnePlace','OnePlace__Relationship__c';
GO

SELECT *
INTO OnePlace__Relationship__c_BackupBeforeDelete
FROM OnePlace__Relationship__c
GO


-- Create a temporary table for the keys to be used for deletion.
IF OBJECT_ID('dbo.OnePlace__Relationship__c_Delete', 'U') IS NOT NULL
    DROP TABLE dbo.OnePlace__Relationship__c_Delete;

CREATE TABLE [dbo].OnePlace__Relationship__c_Delete(
	[Id] [nchar](18) NULL,
	[Error] [nvarchar](255) NULL
) ON [PRIMARY]

INSERT INTO [dbo].OnePlace__Relationship__c_Delete
(
			[Id]
)
SELECT	OpM.ID AS [ID]
FROM	
        oneplace...OnePlace__Relationship__c AS OpM
WHERE
	CreatedByID = (select id from oneplace...[user] where firstname = 'Integration')
AND 
	LastModifiedById = (select id from oneplace...[user] where firstname = 'Integration')
AND
	OnePlace__Relationship_Type__c = ( SELECT ID FROM OnePlace__Relationship_Type__c WHERE [Name] = 'Known By' )


-- Execute the delete (for security this line is commented out until required.)
-- EXEC sf_bulkops 'Delete','ONEPLACE','OnePlace__Relationship__c_Delete'