
-- Create a backup of the table first.
EXEC SF_Replicate 'OnePlace','OnePlace__Relationship__c';
GO

EXEC sp_rename 'OnePlace__Relationship__c', 'OnePlace__Relationship__c_BACKUP';  
GO 

-- Create a temporary table for the keys to be used for deletion.
IF OBJECT_ID('dbo.OnePlace__Relationship__c_Update', 'U') IS NOT NULL
    DROP TABLE dbo.OnePlace__Relationship__c_Update;

CREATE TABLE [dbo].OnePlace__Relationship__c_Update(
	[Id] [nchar](18) NULL,
	[Error] [nvarchar](255) NULL,
	[RecordTypeId] [nchar](18) NULL
) ON [PRIMARY]


INSERT INTO [dbo].OnePlace__Relationship__c_Update
(
	[Id],
	[RecordTypeId]
)
select 
	r.ID, 
	nt.Id
from 
	oneplace...OnePlace__Relationship__c r
INNER JOIN
	oneplace...OnePlace__Relationship_Type__c rt ON rt.Id = r.OnePlace__Relationship_Type__c
INNER JOIN
	oneplace...RecordType t ON t.Id = r.RecordTypeId AND t.sobjectType = 'OnePlace__Relationship__c'
INNER JOIN
	oneplace...RecordType nt ON nt.Name = rt.Name
WHERE
	rt.Name <> t.Name

-- Execute the delete (for security this line is commented out until required.)
-- EXEC sf_bulkops 'Update','ONEPLACE','OnePlace__Relationship__c_Update'